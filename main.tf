terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

provider "yandex" {
  zone = "ru-central1-a"
}

resource "yandex_compute_disk" "hdd_disk" {
  type = "network-hdd"
  zone = "ru-central1-a"

  folder_id = "b1g8b26higpu14bsnfu8"
}

resource "yandex_compute_instance" "vm_1" {
  name = "terraform1"

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = "fd8g1jhgpuach7k158lh"
    }
  }

  secondary_disk {
    disk_id = "${yandex_compute_disk.hdd_disk.id}"
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.subnet_1.id
    nat       = true
  }

  metadata = {
    user-data = "${file("D:/devops-terra/cloud-terraform/user-data.txt")}"
  }
}

resource "yandex_vpc_network" "network_1" {
  name = "network1"
}

resource "yandex_vpc_subnet" "subnet_1" {
  name           = "subnet1"
  zone           = "ru-central1-a"
  network_id     = "${yandex_vpc_network.network_1.id}"
  v4_cidr_blocks = ["192.168.10.0/24"]
}